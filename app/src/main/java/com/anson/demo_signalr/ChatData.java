package com.anson.demo_signalr;

/**
 * Created by Anson on 2015/8/23.
 */
public class ChatData {
    private String name;
    private String message;

    public ChatData(String name, String message) {
        this.name = name;
        this.message = message;
    }

    public String getName() {
        return name;
    }
    public String getMessage() {
        return message;
    }
}
